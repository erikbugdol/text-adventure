package Combatmoves;

public class CombatMove {
    private int basedamage = 10;        //basedamage of combatmove
    private int level=1;
    private int selfdamage=0;
    private String name;

    private int damage;

    public CombatMove(String name, int basedamage){
        this.basedamage=basedamage;
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CombatMove(String name){
        this.name=name;


    }

    public int getBasedamage() {
        return basedamage;
    }

    public void setBasedamage(int basedamage) {
        this.basedamage = basedamage;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int leve) {
        this.level = leve;
    }

    public int getSelfdamage() {
        return selfdamage;
    }

    public void setSelfdamage(int selfdamage) {
        this.selfdamage = selfdamage;
    }
}
