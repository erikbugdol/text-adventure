package Combatmoves;

public class Spell extends CombatMove{
    private int basemanacost = 10;     //changed by level

    private int manacost;
    public Spell(String name, int basedamage){
        super(name,basedamage);
    }

    public int getManacost() {
        return (int)(super.getLevel()/3)*basemanacost;
    }
}
