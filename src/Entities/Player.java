package Entities;

public class Player extends Entity {

    public Player(String name){
        super(name);
    }

    private int skillpoints = 0;

    public int getSkillpoints() {
        return skillpoints;
    }

    public void setSkillpoints(int skillpoints) {
        this.skillpoints = skillpoints;
    }



    private void notenoughSkillpoints(){
        System.out.println("You do not have enough Skillpoints to do that right now.");
    }


    public void skillStrength(int amount){
        if(skillpoints<amount){
            notenoughSkillpoints();
        }
        else{
            skillpoints-=amount;
            super.setStrength(super.getStrength()+amount);
        }
    }
    public void skillIntelligence(int amount){
        if(skillpoints<amount){
            notenoughSkillpoints();
        }
        else{
            skillpoints-=amount;
            super.setIntelligence(super.getIntelligence()+amount);
        }
    }
    public void skillWisdom(int amount){
        if(skillpoints<amount){
            notenoughSkillpoints();
        }
        else{
            skillpoints-=amount;
            super.setWisdom(super.getWisdom()+amount);
        }
    }
    public void skillstamina(int amount){
        if(skillpoints<amount){
            notenoughSkillpoints();
        }
        else{
            skillpoints-=amount;
            super.setStamina(super.getStamina()+amount);
        }
    }
}
