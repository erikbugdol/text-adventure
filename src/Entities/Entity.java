package Entities;

import Combatmoves.CombatMove;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Entity {
    private static int basehealth=100;           //basehealth changed with equipment and stamina
    private static int basemana=500;            //basemana changed with equipment and wisdom
    private int maxhealth=100;
    private int maxmana=500;

    //Skills
    private int strength=10;                     //changes damage of attacks
    private int intelligence=10;                 //changes damage of spells
    private int wisdom=10;                       //changes amount of mana
    private int stamina=10;                      //changes amount of health

    private int armour=0;
    private int itemstrength=0;
    private int itemintelligence=0;
    private int itemwisdom=0;
    private int itemstamina=0;

    private Items.Weapon meeleeweapon = new Items.Weapon(10);
    private Items.Weapon mageweapon = new Items.Weapon(10);


    private int health=1000;
    private int mana=500;

    private String name;

    private ArrayList<CombatMove> combatmoves = new ArrayList<>();

    public Entity(String name){
        this.name= name;
    }


    public ArrayList<CombatMove> getCombatmoves() {
        return combatmoves;
    }

    public void setCombatmoves(ArrayList<CombatMove> combatmoves) {
        this.combatmoves = combatmoves;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getWisdom() {
        return wisdom;
    }

    public void setWisdom(int wisdom) {
        this.wisdom = wisdom;
    }

    public int getStamina() {
        return stamina;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void addCombatmove(CombatMove combatMove){
        combatmoves.add(combatMove);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Entities.Entity{" +
                "name='" + name +
                ", strength=" + strength +
                ", armour=" + armour +
                ", health=" + health + '\'' +
                '}';
    }

    public void takedamage(int amount){
        this.health-=amount;
    }
    public void heal(int amount){
        this.health+=amount;
    }

    public boolean usecombatmove(CombatMove combatmove, Entity target){
        int damage = getmovedamage(combatmove);
        int selfdamage = getselfdamage(combatmove);
        if(combatmove instanceof Combatmoves.Spell){
            if(mana<((Combatmoves.Spell)(combatmove)).getManacost()){
                System.out.println("You do not have enough mana to cast that spell.");
                return false;
            }
            else{
                mana-=((Combatmoves.Spell)(combatmove)).getManacost();
                target.takedamage(damage);
                this.takedamage(selfdamage);
            }
        }
        else{
            target.takedamage(damage);
        }
        return true;


    }

    private int getselfdamage(CombatMove combatmove){
        int selfdamage=combatmove.getSelfdamage();
        if(selfdamage<0){
            return (int)((selfdamage + selfdamage*0.01*itemintelligence)*intelligence);
        }
        else{
            return selfdamage;
        }
    }


    private int getmovedamage(CombatMove combatmove){
        if (combatmove instanceof Combatmoves.Spell){
            return (int)((combatmove.getBasedamage() + combatmove.getBasedamage()*0.01*itemintelligence*mageweapon.getDamage())*intelligence);
        }
        else {
            return (int)((combatmove.getBasedamage() + combatmove.getBasedamage()*0.01*itemstrength*meeleeweapon.getDamage())*strength);
        }
    }
}
