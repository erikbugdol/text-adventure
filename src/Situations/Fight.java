package Situations;

import Combatmoves.CombatMove;
import Entities.Entity;

import java.nio.channels.SelectableChannel;
import java.util.Scanner;

import java.util.ArrayList;
import java.util.Random;

public class Fight{

    private Entities.Player player;
    private Entities.Entity enemy;

    public Fight(Entities.Player player, Entities.Entity enemy){
        this.player=player;
        this.enemy=enemy;
    }

    public void fight(){



        System.out.println("Your enemy is: "  + enemy.toString());

        userattack();

        if(enemy.getHealth()>0){
            enemyattack();
            if(player.getHealth()<0){
                System.out.printf("You died.");
                System.exit(0);
            }
            else{
                fight();
            }
        }
        else {
            System.out.println("You crushed the " + enemy.getName());
        }

    }

    private void enemyattack(){

        ArrayList<CombatMove> combatmoves = enemy.getCombatmoves();

        Random rand = new Random();
        boolean worked = false;
        CombatMove selectedSkill = null;
        do{
            selectedSkill = combatmoves.get(rand.nextInt(combatmoves.size()));

            worked = enemy.usecombatmove(selectedSkill, player);
        } while (!worked);
        System.out.println("Enemy used skill " + selectedSkill.getName());
    }


    private void userattack(){
        Scanner sc = new Scanner(System.in);
        System.out.printf("You can: ");

        ArrayList<CombatMove> combatmoves = player.getCombatmoves();
        for (CombatMove combatmove : combatmoves) {
            System.out.printf(" " + combatmove.getName());
        }



        boolean worked = false;
        do{
            CombatMove selectedSkill = null;
            boolean inputincorrect=true;


            do{
                String input=sc.nextLine();
                for (CombatMove combatmove : combatmoves) {
                    if(!(input==combatmove.getName())){
                        System.out.println("sthelse");
                        inputincorrect &= false;
                        selectedSkill=combatmove;
                    }
                }
            } while(inputincorrect);
            System.out.printf("sth");

            worked = player.usecombatmove(selectedSkill, enemy);
        } while (!worked);
    }


}
