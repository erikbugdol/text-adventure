package Situations;

public class Situation {

    private Situation[] nextactions;
    private String[] nextactiondescriptions;
    private boolean end;
    private String situationdescription;


    public Situation(String situationdescription, Situation[] nextactions, String[] nextactiondescriptions, boolean end){
        this.nextactions=nextactions;
        this.nextactiondescriptions=nextactiondescriptions;
        this.end=end;
        this.situationdescription=situationdescription;
    }

    public Situation[] getNextactions() {
        return nextactions;
    }

    public void setNextactions(Situation[] nextactions) {
        this.nextactions = nextactions;
    }

    public String[] getNextactiondescriptions() {
        return nextactiondescriptions;
    }

    public void setNextactiondescriptions(String[] nextactiondescriptions) {
        this.nextactiondescriptions = nextactiondescriptions;
    }

    public boolean isEnd() {
        return end;
    }

    public void setEnd(boolean end) {
        this.end = end;
    }

    public String getSituationdescription() {
        return situationdescription;
    }

    public void setSituationdescription(String situationdescription) {
        this.situationdescription = situationdescription;
    }
}
