package Items;

public class Weapon {
    private int basedamage=12;

    public Weapon(int basedamage){
        this.basedamage=basedamage;
    }

    public int getDamage() {
        return basedamage;
    }

    public void setDamage(int basedamage) {
        this.basedamage = basedamage;
    }
}
