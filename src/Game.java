
import Combatmoves.CombatMove;
import Entities.Entity;
import Entities.Player;
import Situations.Fight;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;  // Import the Scanner class


public class Game {
    public static void print(String toprint){
        System.out.println(toprint);
    }
    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);

        Map<String, CombatMove> attacks = new HashMap<String, CombatMove>();
        createCombatmoves(attacks);


        print("Welcome Player!");
        print("");
        print("Tell me your name: ");
        String Playername = sc.nextLine();

        Player player = new Player(Playername);
        player.addCombatmove(attacks.get("standardattack"));

        print("");
        print("OK, " + player.getName() + ", Let's start our Adventure!");

        print("You stand on a dirt road heading south/north on your left is a Forest, on your right a wide river.");

        Entity enemy = new Entity("Ork");
        enemy.setStrength(20);
        enemy.addCombatmove(attacks.get("standardattack"));
        print("You encountered an enemy");

        Fight myfight = new Fight(player, enemy);
        myfight.fight();
    }
    public static void createCombatmoves(Map<String, CombatMove> map){
        CombatMove standardattack = new CombatMove("standardattack", 10);
        map.put(standardattack.getName(),standardattack);
    }
}
